require 'rubygems'
require 'xmpp4r'
require 'xmpp4r/roster'
require 'xmpp4r/client'
require 'time'
include Jabber # Makes using it a bit easier as we don't need to prepend Jabber:: to everything

class PingPong

  attr_accessor :times
  attr_accessor :factor

  def initialize
    @client = Client.new(JID::new("ping@ami.www.techfak.uni-bielefeld.de"))
    @client.connect
    @client.auth("ping")
    @client.send(Presence.new.set_type(:available))
    @roster = Roster::Helper.new(@client)
    addcallbacks
    @times = 0
    @time = 0
    @factor = 1000
    puts "Connected!"
  end

  def addcallbacks
    messagecallback
    rostercallback
  end

  def messagecallback
    @client.add_message_callback do |m|
      if @times >= @factor
        puts "Time in ms: #{@time/@factor}"
        @time = 0
        @times = 0
      end
      if @times < @factor
        @times += 1
        @time += (Time.now - Time.parse(m.body))
      end
    end
  end

  def rostercallback
    @roster.add_subscription_request_callback do |item,pres|
      @roster.accept_subscription(pres.from)
    end
  end

  def send user, message
    msg = Message::new(user, message)
    msg.type=:chat
    @client.send(msg)
  end

end

pp = PingPong.new

while true
  tts = 1/pp.factor
  sleep tts
  #pp.send "pong@ami.www.techfak.uni-bielefeld.de", "#{Time.now}"
end

